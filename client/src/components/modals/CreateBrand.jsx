import React from "react";
import { Form } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

const CreateBrand = ({show, onHide}) =>{
    return(
      <Modal
      show={show}
      centered
      onHide={onHide}
   
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Добавить тип
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
      </Modal.Body>
          <Form>
            <Form.Control
              placeholder={"Введите название типа"}
              />
          </Form>
      <Modal.Footer>
        <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
        <Button variant="outline-success" onClick={onHide}>Добавить</Button>
      </Modal.Footer>
    </Modal>
    )
}
export default CreateBrand;
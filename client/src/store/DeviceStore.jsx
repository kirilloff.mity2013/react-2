import {makeAutoObservable} from "mobx";

export default class DeviceStore{
    constructor(){
        this._types = [
            {id: 1, name:"Системные блоки"},
            {id: 2, name:"Мониторы"},
            {id: 3, name:"Кассы"},
            {id: 4, name:"Рутокены"},
            {id: 5, name:"Клавиатуры"},
            {id: 6, name:"Компьютерные мыши"},
        ]
        this._brands = [
            {id: 1, name:"Samsung"},
            {id: 2, name:"Apple"},
            {id: 3, name:"Samsung"},
            {id: 4, name:"Apple"},
        ]
        this._devices = [
            {id: 1, name:"Iphone 12 pro", price:25000, rating:5, img:''},
            {id: 2, name:"Iphone 12 pro", price:25000, rating:5, img:''},
            {id: 3, name:"Iphone 12 pro", price:25000, rating:5, img:''},
        ]
        this._selectedBrand = {}
        this._selectedType = {}
        makeAutoObservable(this)
    }
    setTypes(types){
        this._types = types
    }
    setBrands(brands){
        this._brands = brands
    }
    setDevices(devices){
        this._devices = devices
    }
    setSelectedType(type){
        this._selectedType = type
    }
    setSelectedBrand(brand){
        this._selectedBrand = brand
    }
    get types(){
        return this._types
    }
    get brands(){
        return this._brands
    }
    get devices(){
        return this._devices
    }
    get selectedType(){
        return this._selectedType
    }
    get selectedBrand(){
        return this._selectedBrand
    }
}
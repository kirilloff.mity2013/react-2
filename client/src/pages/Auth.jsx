import React from "react";
import { Button, Card, Container, Form} from "react-bootstrap";
import { LOGIN_ROUTE, REGISTRATIN_ROUTE } from "../utils/consts";
import {NavLink, useLocation} from "react-router-dom";

const Auth = () => {
    const Location = useLocation()
    const isLogin = Location.pathname === LOGIN_ROUTE
    return(
        <Container
        className="d-flex justify-content-center align-items-center"
        style={{height:window.innerHeight - 54}}
        > 
            <Card style={{width:600}} className="p-5">
                <h2 className="m-auto">{isLogin ? 'Авторизация':'Регистрация'}</h2>
                <Form className="d-flex flex-column">
                    <Form.Control
                    className="mt-3"
                    placeholder="Введиите ваш email..."
                    />
                    <Form.Control
                    className="mt-3"
                    placeholder="Введиите ваш пароль..."
                    />
                    <Form className="d-flex justify-content-between mt-3 pl-3 pr-3">
                        {isLogin ?
                        <div>
                            нет аккауна ? <NavLink to={REGISTRATIN_ROUTE}>Зарегестрируйтесь!</NavLink>
                        </div>
                        :
                        <div>
                            Есть аккауна ? <NavLink to={LOGIN_ROUTE}>Войдите</NavLink>
                        </div>
                        }
                        <Button 
                            variant={"outline-success"}>
                            {isLogin ? "Войти" : "Регистрация"}
                        </Button>
                    </Form>
                    
                </Form>

            </Card>
        </Container>
    )
};
export default Auth;
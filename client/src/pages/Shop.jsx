import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import TypeBar from "../components/TypeBar";
import BrandBar from "../components/BrandBar";
import DeviceList from "../components/DeviceList";

const Shop = () =>{
    return(
        <div>
            <Container className="p-3">
                <Row>
                    <Col md={3}>
                    <TypeBar/>
                    </Col>

                    <Col md={5}>
                    <BrandBar/>
                    <DeviceList/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default Shop;
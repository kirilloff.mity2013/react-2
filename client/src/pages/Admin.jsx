import React from 'react';
import { Button, Container } from 'react-bootstrap';
import CreateBrand from '../components/modals/CreateBrand';
import CreateTypes from '../components/modals/CreateTypes'
import CreateDevice from '../components/modals/CreateDevice';

const Admin = () =>{
    return (
      <Container className='d-flex flex-column'>
        <Button variant={"outline-dark"} className="mt-2 p-2">
            Добавить тип
            </Button>
        <Button variant={"outline-dark"} className="mt-2 p-2">
            Добавить устройство
            </Button>
        <Button variant={"outline-dark"} className="mt-2 p-2">
            Добавить бренд
            </Button>
            <CreateBrand/>
            <CreateDevice />
            <CreateTypes show={true}/>
      </Container>
      

    );
};

export default Admin;